<!DOCTYPE html>
<html lang="en">
<head>
	<title>Add New Contact</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>bootstrap/css/bootstrap.min.css">
    <style type="text/css">
    	.col-md-8{
    		margin-bottom: 15px;
    	}
    	@media (min-width: 992px){
			.container {
			    width: 870px;
			}
		}

    </style>
</head>
<body>
<div class="container">
	<h1>Add New Contact</h1>
	<?php echo $message;?>
	<?php echo form_open("add");?>
	<fieldset>

	<div class="form-group">
	  <label class="col-md-4 control-label" for="contact_first_name">First Name: </label>  
	  <div class="col-md-8">
	  	<?php echo form_input($contact_first_name);?>
	  </div>
	</div>

	<div class="form-group">
	  <label class="col-md-4 control-label" for="contact_first_name">Last Name: </label>  
	  <div class="col-md-8">
	  	<?php echo form_input($contact_last_name);?>
	  </div>
	</div>

	<div class="form-group">
	  <label class="col-md-4 control-label" for="contact_first_name">Phone Number: </label>  
	  <div class="col-md-8">
	  	<?php echo form_input($contact_phone);?>
	  </div>
	</div>

	<div class="form-group">
	  <label class="col-md-4 control-label" for="contact_first_name">Email Address: </label>  
	  <div class="col-md-8">
	  	<?php echo form_input($contact_email);?>
	  </div>
	</div>

	</fieldset>
	<?php echo form_button(array(
		'name' => 'submit',
	    'id' => 'submit',
	    'value' => 'submit',
	    'type' => 'submit', 
	    'content' => '<span class="glyphicon glyphicon-send"></span> Submit',
	    'style'=>"float:right; margin-top: 20px; margin-bottom: 10px; margin-left: 10px; margin-right: 10px;",
		'class'=>'btn btn-primary btn-lg'
		));
	?>
	<input type="button" name="btnBack" id="btnBack" value="Back" style="float:right; margin-top: 20px; margin-bottom: 10px;" class="btn btn-default btn-lg" onclick="window.location.href='<?php echo base_url() ?>'" />
	<?php echo form_close(); ?>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    setTimeout(function() {
	        $(".alert").remove();
	    }, 2000);
	});
</script>
</html>
