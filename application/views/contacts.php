<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Phone Book</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>bootstrap/css/bootstrap.min.css">
    <style type="text/css">
    .btn-addnew {
      margin: auto;
      margin-top: 10px;
      margin-bottom: 10px;
    }
    @media (min-width: 992px){
      .btn-addnew {
        float: right;
        margin: 10px;
      }
      h1 {
        display: inline-block;
      }
    }
      
    </style>
  </head>
  <body>
    <div class="container">
      <h1>Phone Book With Code Igniter</h1>
      <a href="<?php base_url(); ?>add" class="btn btn-lg btn-primary btn-addnew"><span class="glyphicon glyphicon-plus"></span> Add New Contact</a>
      <?php echo $message;?>
      <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <thead>
           <th>
            <td><strong>First Name</strong></td>
            <td><strong>Last Name</strong></td>
            <td><strong>Phone Number</strong></td>
            <td><strong>Email Address</strong></td>
            <td><strong>Edit</strong></td>
            <td><strong>Delete</strong></td>
          </th>
        </thead>
          <tbody>
           <?php 
             $index = 1;
             foreach($contacts as $contact) {
           ?>
           <tr>
               <td><?php echo $index; ?></td>
               <td><?php echo $contact->contact_first_name;?></td>
               <td><?php echo $contact->contact_last_name;?></td>
               <td><?php echo $contact->contact_phone;?></td>
               <td><?php echo $contact->contact_email;?></td>
               <td><a type="button" class="btn btn-success" href='edit/<?php echo $contact->contact_id ?>'><i class="glyphicon glyphicon-pencil"></i></a></td>
               <td>
                  <?php 
                    echo anchor(
                      'delete/'.$contact->contact_id,
                      '<i class="glyphicon glyphicon-trash"></i>',
                      array(
                        'onClick' => "return confirm('Are you sure you want to delete $contact->contact_first_name $contact->contact_last_name from your contact list?')",
                        'class' => "btn btn-danger"
                        )
                    );
                  ?>
               </td>
            </tr>    
           <?php $index++; }?>  
          </tbody>
        </table>
      </div>
    </div>
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            $(".alert").remove();
        }, 2000);
    });
  </script>
</html>