<?php
	class Contact extends CI_Controller {

		function __Construct(){
		  parent::__Construct ();
		   $this->load->database(); // load database
		   $this->load->model('ContactModel'); // load model
		   $this->load->library('form_validation');
		}
		 
		public function index() {
		   $this->data['contacts'] = $this->ContactModel->getContacts(); // calling Contact model method getContacts()
		   $this->data['message'] = $this->session->flashdata('message');
		   $this->load->view('contacts', $this->data); // load the view file , we are passing $data array to view file
		}

		public function addContact() { 
			//validate form input
			$this->form_validation->set_rules('contact_first_name', 'First Name', 'required|xss_clean');
			$this->form_validation->set_rules('contact_last_name', 'Last Name', 'required|xss_clean');
			$this->form_validation->set_rules('contact_phone', 'Phone Number', 'required|xss_clean');
			$this->form_validation->set_rules('contact_email', 'Email', 'required|xss_clean');

			if ($this->form_validation->run() == true)
			{		
				$data = array(
					'contact_first_name'	=> $this->input->post('contact_first_name'),
					'contact_last_name'		=> $this->input->post('contact_last_name'),
					'contact_phone' 		=> $this->input->post('contact_phone'),
					'contact_email'  		=> $this->input->post('contact_email')
				);
				
				$this->ContactModel->insertContact($data);
				
				$this->session->set_flashdata('message', '<div class="alert alert-dismissible alert-success" role="alert">Contact added successfully.</div>');
				
				redirect(base_url());
			}else{
				//display the add contact form
				//set the flash data error message if there is one
				$this->data['message'] = (validation_errors() ? validation_errors('<div class="alert alert-danger" role="alert">', '</div>') : $this->session->flashdata('message'));

				$this->data['contact_first_name'] = array(
					'name'  	=> 'contact_first_name',
					'id'    	=> 'contact_first_name',
					'type'  	=> 'text',
					'class'		=> 'form-control input-md',
					'placeholder'=> 'First Name',
					'value' 	=> $this->form_validation->set_value('First Name'),
				);			
				$this->data['contact_last_name'] = array(
					'name'  	=> 'contact_last_name',
					'id'    	=> 'contact_last_name',
					'type'  	=> 'text',
					'class'		=> 'form-control input-md',
					'placeholder'=> 'Last Name',
					'value' 	=> $this->form_validation->set_value('Last Name'),
				);
				$this->data['contact_phone'] = array(
					'name'  	=> 'contact_phone',
					'id'    	=> 'contact_phone',
					'type'  	=> 'text',
					'class'		=> 'form-control input-md',
					'placeholder'=> '0123456789',
					'value' 	=> $this->form_validation->set_value('Phone Number'),
				);
				$this->data['contact_email'] = array(
					'name'  => 'contact_email',
					'id'    => 'contact_email',
					'type'  => 'text',
					'class'		=> 'form-control input-md',
					'placeholder'=> 'yourname@email.com',
					'value' => $this->form_validation->set_value('Email'),
				);
				
				$this->load->view('addContact', $this->data);
			}
		}

		public function editContact($contact_id) {

			$contact = $this->ContactModel->getContact($contact_id);

			//validate form input
			$this->form_validation->set_rules('contact_first_name', 'First Name', 'required|xss_clean');
			$this->form_validation->set_rules('contact_last_name', 'Last Name', 'required|xss_clean');
			$this->form_validation->set_rules('contact_phone', 'Phone Number', 'required|xss_clean');
			$this->form_validation->set_rules('contact_email', 'Email', 'required|xss_clean');
		
			if (isset($_POST) && !empty($_POST))
			{		
				$data = array(
					'contact_first_name'	=> $this->input->post('contact_first_name'),
					'contact_last_name'		=> $this->input->post('contact_last_name'),
					'contact_phone' 		=> $this->input->post('contact_phone'),
					'contact_email'  		=> $this->input->post('contact_email')
				);

				if ($this->form_validation->run() === true)
				{
					$this->ContactModel->updateContact($contact_id, $data);

					$this->session->set_flashdata('message', '<div class="alert alert-dismissible alert-success" role="alert">Contact updated successfully.</div>');
					
					redirect(base_url().'edit/'.$contact_id);
				}
			}

			$this->data['message'] = (validation_errors() ? validation_errors('<div class="alert alert-dismissible alert-danger" role="alert">', '</div>') : $this->session->flashdata('message'));
			
			$this->data['contact'] = $contact;
			
			//display the edit contact form
			$this->data['contact_first_name'] = array(
				'name'  	=> 'contact_first_name',
				'id'    	=> 'contact_first_name',
				'type'  	=> 'text',
				'class'		=> 'form-control input-md',
				'value' 	=> $this->form_validation->set_value('contact_first_name', $contact['contact_first_name'])
			);			
			$this->data['contact_last_name'] = array(
				'name'  	=> 'contact_last_name',
				'id'    	=> 'contact_last_name',
				'type'  	=> 'text',
				'class'		=> 'form-control input-md',
				'value' 	=> $this->form_validation->set_value('contact_last_name', $contact['contact_last_name'])
			);
			$this->data['contact_phone'] = array(
				'name'  	=> 'contact_phone',
				'id'    	=> 'contact_phone',
				'type'  	=> 'text',
				'class'		=> 'form-control input-md',
				'value' 	=> $this->form_validation->set_value('contact_phone', $contact['contact_phone'])
			);
			$this->data['contact_email'] = array(
				'name'  => 'contact_email',
				'id'    => 'contact_email',
				'type'  => 'text',
				'class'		=> 'form-control input-md',
				'value' 	=> $this->form_validation->set_value('contact_email', $contact['contact_email'])
			);
			
			$this->load->view('editContact', $this->data);
		}	
		
		function deleteContact($contact_id) {
			$this->ContactModel->deleteContact($contact_id);
			$this->session->set_flashdata('message', '<div class="alert alert-dismissible alert-success" role="alert">Contact were successfully deleted!</div>');
			redirect(base_url());
		}
	}
?>