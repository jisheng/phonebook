<?php
	class ContactModel extends CI_Model {
	 
		function getContacts(){
		  $this->db->select("contact_id, contact_first_name, contact_last_name, contact_phone, contact_email");
		  $this->db->from('contact_table');
		  $query = $this->db->get();
		  return $query->result();
		}

		function getContact($contact_id) {
			$this->db->select('contact_id, contact_first_name, contact_last_name, contact_phone, contact_email');
			$this->db->where('contact_id', $contact_id);
			$query = $this->db->get('contact_table');
			return $query->row_array();
		}

		function insertContact($data)
		{
			$this->db->insert('contact_table', $data);
			$id = $this->db->insert_id();
			return (isset($id)) ? $id : FALSE;
		}

		function updateContact($contact_id, $data)
		{
			$this->db->where('contact_id', $contact_id);
			$this->db->update('contact_table', $data);
		}
		
		function deleteContact($contact_id)
		{
			$this->db->where('contact_id', $contact_id);
			$this->db->delete('contact_table');
		}
	 
	}
?>